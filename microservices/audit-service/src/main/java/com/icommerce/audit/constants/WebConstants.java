package com.icommerce.audit.constants;

public interface WebConstants {

    /**
     * View Constants
     *
     * @author Tong Thanh Vinh
     */
    public interface View {
        String AUDIT = "audit";
    }
}

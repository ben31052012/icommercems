package com.icommerce.catalog.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.icommerce.catalog.dto.InventoryItemDto;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ProductInventoryService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${app.inventory-service.host}")
	private String inventoryServiceUri;
	
	// Demo SYCN request to inventory service via Discovery Service name
	@HystrixCommand(fallbackMethod = "fallback")
	public int getInventory(@PathVariable("id") final Long productId) {
		LOG.info("getting inventory object ... ");
		// get Basket
		InventoryItemDto inventory = restTemplate.getForObject(inventoryServiceUri + "/" + productId,
				InventoryItemDto.class);
		LOG.info("Returning inventory ... ");
		return inventory.getAvailableQuantity();
	}

	// a fallback method to be called if failure happened
	public int fallback(Long productId) {
		LOG.info("Returning fall ... ");
		return 0;
	}
}

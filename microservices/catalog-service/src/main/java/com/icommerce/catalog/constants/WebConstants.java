package com.icommerce.catalog.constants;

public interface WebConstants {

    /**
     * View Constants
     *
     */
    public interface View {
        String USER = "user";

        String PRODUCT = "product";

        String AUTH = "auth";
    }
    
}
